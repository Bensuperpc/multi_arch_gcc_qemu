# Use -static if your computer (hsot) doesn't have compiled lib and remove -L /usr/*
export WINEDEBUG=-all

#docker build -no-cache . -t gcc_toolchain:lastest
docker build -f Dockerfiles/Dockerfile_gcc_all . -t gcc_toolchain:lastest
#docker build -f Dockerfiles/Dockerfile_gcc_all_low . -t gcc_toolchain_low:lastest

echo "Compile GCC: x86_32"
docker run --rm -it -v "$(pwd)/build":"/tmp/gcc" -v "$(pwd)/main.c":"/tmp/gcc/main.c" gcc_toolchain:lastest gcc-10 -static -m32 -O3 -march=native /tmp/gcc/main.c -o /tmp/gcc/main_x86_32
echo "Compile GCC: x86_64"
docker run --rm -it -v "$(pwd)/build":"/tmp/gcc" -v "$(pwd)/main.c":"/tmp/gcc/main.c" gcc_toolchain:lastest gcc-10 -static -m64 -O3 -march=native /tmp/gcc/main.c -o /tmp/gcc/main_x86_64
echo "Compile Clang: x86_32"
docker run --rm -it -v "$(pwd)/build":"/tmp/gcc" -v "$(pwd)/main.c":"/tmp/gcc/main.c" gcc_toolchain:lastest clang-10 -m32 -static -O3 -march=native /tmp/gcc/main.c -o /tmp/gcc/main_x86_32_clang
echo "Compile Clang: x86_64"
docker run --rm -it -v "$(pwd)/build":"/tmp/gcc" -v "$(pwd)/main.c":"/tmp/gcc/main.c" gcc_toolchain:lastest clang-10 --target=x86_64 -static -O3 -march=native /tmp/gcc/main.c -o /tmp/gcc/main_x86_64_clang
echo "Compile GCC: x86_64_win"
docker run --rm -it -v "$(pwd)/build":"/tmp/gcc" -v "$(pwd)/main.c":"/tmp/gcc/main.c" gcc_toolchain:lastest x86_64-w64-mingw32-gcc-9 -static -m64 -O3 -march=native /tmp/gcc/main.c -o /tmp/gcc/main_x86_64_win
echo "Compile GCC: aarch32"
docker run --rm -it -v "$(pwd)/build":"/tmp/gcc" -v "$(pwd)/main.c":"/tmp/gcc/main.c" gcc_toolchain:lastest arm-linux-gnueabihf-gcc-10 -static -O3 /tmp/gcc/main.c -o /tmp/gcc/main_aarch32
echo "Compile Clang: aarch32"
docker run --rm -it -v "$(pwd)/build":"/tmp/gcc" -v "$(pwd)/main.c":"/tmp/gcc/main.c" gcc_toolchain:lastest clang-10 --target=arm-linux-gnueabihf -static -O3 /tmp/gcc/main.c -o /tmp/gcc/main_aarch32_clang
echo "Compile GCC: aarch64"
docker run --rm -it -v "$(pwd)/build":"/tmp/gcc" -v "$(pwd)/main.c":"/tmp/gcc/main.c" gcc_toolchain:lastest aarch64-linux-gnu-gcc-10 -static -O3 /tmp/gcc/main.c -o /tmp/gcc/main_aarch64
echo "Compile Clang aarch64"
docker run --rm -it -v "$(pwd)/build":"/tmp/gcc" -v "$(pwd)/main.c":"/tmp/gcc/main.c" gcc_toolchain:lastest clang-10 --target=aarch64-linux-gnu -static -O3 /tmp/gcc/main.c -o /tmp/gcc/main_aarch64_clang
#echo "Compile GCC: riscv32"
#docker run --rm -it -v "$(pwd)/build":"/tmp/gcc" -v "$(pwd)/main.c":"/tmp/gcc/main.c" gcc_toolchain:lastest riscv64-linux-gnu-gcc-10 -static -O3 /tmp/gcc/main.c -o /tmp/gcc/main_riscv32
echo "Compile GCC: riscv64"
docker run --rm -it -v "$(pwd)/build":"/tmp/gcc" -v "$(pwd)/main.c":"/tmp/gcc/main.c" gcc_toolchain:lastest riscv64-linux-gnu-gcc-10 -static -O3 /tmp/gcc/main.c -o /tmp/gcc/main_riscv64

echo "Compile Clang: mips"
docker run --rm -it -v "$(pwd)/build":"/tmp/gcc" -v "$(pwd)/main.c":"/tmp/gcc/main.c" gcc_toolchain:lastest clang-10 --target=mips-linux-gnu -static -O3 /tmp/gcc/main.c -o /tmp/gcc/main_mips_clang
echo "Compile Clang: mips64"
docker run --rm -it -v "$(pwd)/build":"/tmp/gcc" -v "$(pwd)/main.c":"/tmp/gcc/main.c" gcc_toolchain:lastest clang-10 --target=mips64-linux-gnuabi64 -static -O3 /tmp/gcc/main.c -o /tmp/gcc/main_mips64_clang

echo "Compile GCC: mips"
docker run --rm -it -v "$(pwd)/build":"/tmp/gcc" -v "$(pwd)/main.c":"/tmp/gcc/main.c" gcc_toolchain:lastest mips-linux-gnu-gcc-10 -static -O3 /tmp/gcc/main.c -o /tmp/gcc/main_mips
echo "Compile GCC: mips64"
docker run --rm -it -v "$(pwd)/build":"/tmp/gcc" -v "$(pwd)/main.c":"/tmp/gcc/main.c" gcc_toolchain:lastest mips64-linux-gnuabi64-gcc-10 -static -O3 /tmp/gcc/main.c -o /tmp/gcc/main_mips64

echo "Compile GCC: m68k"
docker run --rm -it -v "$(pwd)/build":"/tmp/gcc" -v "$(pwd)/main.c":"/tmp/gcc/main.c" gcc_toolchain:lastest m68k-linux-gnu-gcc-10 -static -O3 /tmp/gcc/main.c -o /tmp/gcc/main_m68k
echo "Compile GCC: sparc64"
docker run --rm -it -v "$(pwd)/build":"/tmp/gcc" -v "$(pwd)/main.c":"/tmp/gcc/main.c" gcc_toolchain:lastest sparc64-linux-gnu-gcc-10 -static -O3 /tmp/gcc/main.c -o /tmp/gcc/main_sparc64
echo "Compile GCC: alpha"
docker run --rm -it -v "$(pwd)/build":"/tmp/gcc" -v "$(pwd)/main.c":"/tmp/gcc/main.c" gcc_toolchain:lastest alpha-linux-gnu-gcc-10 -static -O3 /tmp/gcc/main.c -o /tmp/gcc/main_alpha
echo "Compile GCC: OK"

echo "Compile GCC: powerpc"
docker run --rm -it -v "$(pwd)/build":"/tmp/gcc" -v "$(pwd)/main.c":"/tmp/gcc/main.c" gcc_toolchain:lastest powerpc-linux-gnu-g++-10 -static -O3 /tmp/gcc/main.c -o /tmp/gcc/main_powerpc
echo "Compile GCC: powerpc64"
docker run --rm -it -v "$(pwd)/build":"/tmp/gcc" -v "$(pwd)/main.c":"/tmp/gcc/main.c" gcc_toolchain:lastest powerpc64-linux-gnu-g++-10 -static -O3 /tmp/gcc/main.c -o /tmp/gcc/main_powerpc64

echo "Compile: OK"

#echo "x86"
#qemu-x86_64 build/main_x86_32
echo "x86-64"
qemu-x86_64 build/main_x86_64
echo "x86-64 clang"
qemu-x86_64 build/main_x86_64_clang
echo "win x86-64"
wine build/main_x86_64_win.exe
echo "aarch32"
qemu-arm build/main_aarch32
echo "aarch32 clang"
qemu-arm build/main_aarch32_clang
echo "aarch64"
qemu-aarch64 build/main_aarch64
echo "aarch64 clang"
qemu-aarch64 build/main_aarch64_clang
#echo "riscv32"
#qemu-riscv32 main_riscv32
echo "riscv64"
qemu-riscv64 build/main_riscv64
echo "mips"
qemu-mips build/main_mips
echo "mips clang"
qemu-mips build/main_mips_clang
echo "mips64"
qemu-mips64 build/main_mips64
echo "mips64 clang"
qemu-mips64 build/main_mips64_clang
echo "m68k"
qemu-m68k build/main_m68k
echo "sparc64"
qemu-sparc64 build/main_sparc64
echo "alpha"
qemu-alpha build/main_alpha
echo "powerpc"
qemu-ppc build/main_powerpc
echo "powerpc64"
qemu-ppc64 build/main_powerpc64

echo "Exec: OK"

